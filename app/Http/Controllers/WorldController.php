<?php
/**
 * World Page Controller
 */

namespace App\Http\Controllers;

use App\Libraries\ServiceRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\WebStory;
use Illuminate\Support\Arr;

/**
 * World Page Controller
 * @package App\Http\Controllers
 */
class WorldController extends Controller
{
    /** @var Content Content model */
    protected $contentModel;
    protected $webStoryModel;

    /**
     * Initialize ContentController class.
     *
     * @param Content $content Users model
     */
    public function __construct( Content $content, WebStory $webStory )
    {
        $this->contentModel  = $content;
        $this->webStoryModel = $webStory;
    }

    /**
     * Display world page.
     *
     * @return Factory|View World page
     */
    public function index( Request $request )
    {
        $webStory      = $this->webStoryModel->getWebStory();
        $contentDetail = $this->contentModel->getHeaderMenu();
        $contentList   = $this->contentModel->getContentList();

        return view( 'world.index', compact( 'contentDetail', 'contentList', 'webStory' ) );
    }

    /**
     * Display world page.
     *
     * @return Factory|View World page
     */
    public function menu( $menuID, Request $request )
    {
        $contentDetail = $this->contentModel->getHeaderMenu();
        $contentList   = $this->contentModel->getContentMenuList( $menuID, $request );

        if( $request->ajax() ){
            return response()->json( [
                                         'data' => view( 'world.menu_list', compact( 'contentList' ) )->render(),
                                     ] );
        }

        return view( 'world.menu', compact( 'contentDetail', 'contentList' ) );
    }

    /**
     * Display world page.
     *
     * @return Factory|View World page
     */
    public function tags( $slug, Request $request )
    {
        $contentDetail = $this->contentModel->getHeaderMenu();
        $contentList   = $this->contentModel->getContentTagsList( $slug, $request );

        if( $request->ajax() ){
            return response()->json( [
                                         'data' => view( 'world.tags_list', compact( 'contentList' ) )->render(),
                                     ] );
        }

        return view( 'world.tags', compact( 'contentDetail', 'contentList', 'slug' ) );
    }

    /**
     * Display world page.
     *
     * @return Factory|View World page
     */
    public function detail( $id )
    {
        $contentDetail = $this->contentModel->getContentDetail( $id );

        $mainImage    = $this->getMainImage( $contentDetail );
        $galleryImage = $this->getGalleryImage( $contentDetail );
        $moreContent  = $this->getMoreContent( $id );

        return view( 'world.detail', compact( 'contentDetail', 'mainImage', 'moreContent', 'galleryImage' ) );
    }

    private function getMainImage( $contentDetail )
    {
        return ServiceRequest::call( 'GET',
                                     '/assets/' . $contentDetail['data'][0]->main_image,
                                     true, );
    }

    private function getGalleryImage( $contentDetail )
    {

        foreach( $contentDetail['image'] as $image ){
            $transformImage  = ServiceRequest::call( 'GET',
                                                     '/assets/' . $image[0]->image,
                                                     true, );
            $image->newImage = $transformImage;
        }

        return $contentDetail['image'];
    }

    private function getMoreContent( $id )
    {
        return $this->contentModel->getMoreContent( $id );
    }

    public function search( Request $request )
    {
        $contentDetail = $this->contentModel->getHeaderMenu();
        $contentList   = $this->contentModel->getContentSearchList( $request );
        $search        = $request->input( 'search' );

        if( $request->ajax() ){
            return response()->json( [
                                         'data' => view( 'world.search_list', compact( 'contentList', 'search' ) )->render(),
                                     ] );
        }

        return view( 'world.search', compact( 'contentDetail', 'contentList', 'search' ) );

    }
}
