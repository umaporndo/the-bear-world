<?php

return [

    /*
    |--------------------------------------------------------------------------
    | World Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index'  => 'The bear world',
        'detail' => 'The bear world',
    ],

    'page_title' => [
        'index'  => 'The bear world',
        'detail' => ':world_title | The bear world',
        'menu'   => 'The bear world | :menu_title',
    ],

    'page_description' => [
        'index'  => 'The bear world',
        'detail' => 'The bear world :world_description',
        'menu'   => 'The bear world | :menu_title',
    ],

    'page_keyword' => [
        'index'  => 'The bear world',
        'detail' => 'The bear world :world_keyword',
        'menu'   => 'The bear world | :menu_title',
    ],

    'og_title' => [
        'index'  => 'The bear world',
        'detail' => ':world_title | The bear world',
        'menu'   => 'The bear world | :menu_title',
    ],

    'og_description' => [
        'index'  => 'The bear world',
        'detail' => ':world_description',
        'menu'   => 'The bear world | :menu_title',
    ],

    'og_keyword' => [
        'index'  => 'The bear world',
        'detail' => ':world_keyword',
        'menu'   => 'The bear world | :menu_title',
    ],

    'og_url' => [
        'index'  => route( 'world.index' ),
        'detail' => route( 'world.detail', [ 'slug' => ':slug', 'id' => ':world_id' ] ),
        'menu'   => route( 'world.menu', [ 'slug' => ':slug', 'menuID' => ':menu_id' ] ),
    ],

];
