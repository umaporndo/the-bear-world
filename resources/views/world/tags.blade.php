@extends('layouts.app')

@section('page-title', __('world.page_title.menu',[
            'menu_title' => '',
        ]))
@section('page-description', __('world.page_description.menu',[
            'menu_title' => '',
        ]))
@section('page-keyword', __('world.page_keyword.menu',[
            'menu_title' => '',
        ]))

@section('og-image', asset(config('images.open_graph.default_image')))
@section('og-title', __('world.og_title.menu',[
            'menu_title' => '',
        ]))
@section('og-description', __('world.og_description.menu',[
            'menu_title' => '',
        ]))
@section('og-keyword', __('world.og_keyword.menu',[
            'menu_title' => '',
        ]))
@section('og-url', __('world.og_url.menu',[
            'menu_id' => '',
            'slug' => ''
        ]) )

@section('content')
    <div class="content-desktop">
    </div>
    <div>
        <div class="container content-detail" style="margin-top:70px;">
            @if($contentList->total() !==  0)
                <h1>{{ $slug }}</h1>
                <div id="content-list-box">
                    @include('world.tags_list')
                </div>
            @else
                <h3>Empty Content</h3>
            @endif

        </div>
    </div>
@endsection
